﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TransportGame.Model;
using TransportGame.Utils;

namespace TransportGame.MapViewer.Storage
{
    public static class MapStorage
    {
        public static CityMap Read(string file)
        {
            return SerializationHelper.DeserializeXml<CityMap>(file);
        }
    }
}
