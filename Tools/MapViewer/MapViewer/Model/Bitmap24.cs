﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TransportGame.MapViewer.Model
{
    public class Bitmap24
    {
        /// <summary>
        /// Gets the width of the bitmap
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Gets the height of the bitmap
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// Gets the raw bytes of the bitmap
        /// </summary>
        public byte[] Raw { get; private set; }

        /// <summary>
        /// Initializes a bitmap
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public Bitmap24(int w, int h)
        {
            Width = w;
            Height = h;

            Raw = new byte[w * h * 3];
        }

        /// <summary>
        /// Gets or sets a pixel
        /// </summary>
        /// <param name="x">x coordinate of pixel</param>
        /// <param name="y">y coordinate of pixel</param>
        public Color this[int x, int y]
        {
            get
            {
                int index = RawIndexOf(x, y);
                return Color.FromRgb(Raw[index], Raw[index + 1], Raw[index + 2]);
            }
            set
            {
                int index = RawIndexOf(x, y);
                Raw[index] = value.R;
                Raw[index + 1] = value.G;
                Raw[index + 2] = value.B;
            }
        }

        private int RawIndexOf(int x, int y)
        {
            if (x < 0 || x >= Width)
                throw new ArgumentOutOfRangeException("x is out of range");

            if (y < 0 || y >= Height)
                throw new ArgumentOutOfRangeException("x is out of range");

            return 3 * (x + y * Width);
        }

        public void DrawLine(int x0, int y0, int x1, int y1, Color color)
        {
            int dx = Math.Abs(x1 - x0);
            int sx = x0 < x1 ? 1 : -1;

            int dy = Math.Abs(y1 - y0);
            int sy = y0 < y1 ? 1 : -1;

            int err = (dx > dy ? dx : -dy) / 2, e2;

            for (; ; )
            {
                this[x0, y0] = color;
                if (x0 == x1 && y0 == y1)
                    break;

                e2 = err;
                if (e2 > -dx) 
                {
                    err -= dy; 
                    x0 += sx; 
                }
                if (e2 < dy) 
                { 
                    err += dx; 
                    y0 += sy; 
                }
            }
        }
    }
}
