﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TransportGame.MapViewer.Model;

namespace TransportGame.MapViewer.Business
{
    public static class BitmapExtensions
    {
        /// <summary>
        /// Gets a wpf bitmap source from a bitmap.
        /// </summary>
        /// <param name="this">The bitmap</param>
        /// <returns>The bitmap source</returns>
        public static BitmapSource ToBitmapSource(this Bitmap24 @this)
        {
            return BitmapSource.Create(@this.Width, @this.Height,
                96, 96, PixelFormats.Rgb24, null, @this.Raw, @this.Width * 3);
        }
    }
}
