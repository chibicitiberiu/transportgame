﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Model;
using TransportGame.Utils;

namespace TransportGame.Business
{
    public static class BiomeManager
    {
        private static List<Biome> biomes = new List<Biome>();

        /// <summary>
        /// Gets all the loaded biomes
        /// </summary>
        public static IEnumerable<Biome> Biomes
        {
            get
            {
                return biomes;
            }
        }

        /// <summary>
        /// Loads the biomes from the Biome directory.
        /// </summary>
        public static void LoadBiomes()
        {
            foreach (var file in Directory.GetFiles(ConfigManager.BiomeDirectory, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    // Try to deserialize biome
                    var biome = SerializationHelper.DeserializeXml<Biome>(file);
                    biome.FileName = file;

                    // Add it to biome list
                    biomes.Add(biome);
                    Logger.Info("Loaded biome '{0}' from file '{1}'.", biome.Name, file);
                }
                catch (Exception ex)
                {
                    Logger.Exception(ex);
                }
            }
        }
    }
}
