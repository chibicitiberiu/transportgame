﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportGame.Generator;
using TransportGame.Model;
using TransportGame.Utils;

namespace TransportGame.Generator
{
    /// <summary>
    /// Complete city generator. Generates everything, from terrain to buildings
    /// </summary>
    public class CityGenerator
    {
        /// <summary>
        /// Generates a city
        /// </summary>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        /// <returns>City</returns>
        public CityMap Generate(int width, int height)
        {
            CityMap map;

            // Generate terrain
            TerrainGenerator terrainGen = new TerrainGenerator();
            map = terrainGen.Generate(width, height);

            // Generate population map
            PopulationCentersGenerator populationGen = new PopulationCentersGenerator();
            populationGen.Generate(map);

            // Generate roads
            RoadGenerator roadGenerator = new RoadGenerator();
            roadGenerator.Generate(map);

            // Generate buildings
            BuildingGenerator buildingGenerator = new BuildingGenerator();
            buildingGenerator.Generate(map);

            // Done
            return map;
        }
    }
}
