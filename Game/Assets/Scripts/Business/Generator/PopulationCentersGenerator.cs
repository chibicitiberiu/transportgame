﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportGame.Model;
using TransportGame.Noise;
using TransportGame.Primitives;

namespace TransportGame.Generator
{
    public class PopulationCentersGenerator
    {
        System.Random random = new System.Random();

        public void Generate(CityMap map)
        {
            // Generate range
            float mp = (float)(map.Width * map.Height) / (1024 * 1024); // For 4k x 4k range should be around 900
            map.PopulationCenterRange = mp * 31f + 496.66f;    // For 2k x 2k range should be around 600

            // Generate a number of points
            int maxPoints = 16 * (int) Math.Sqrt(mp);
            int points = random.Next(maxPoints / 2, maxPoints);

            for (int i = 0; i < points; ++i)
            {
                int px, py;

                do
                {
                    px = random.Next(map.Width / 8, 7 * map.Width / 8);
                    py = random.Next(map.Height / 8, 7 * map.Height / 8);
                }
                while (map.IsWater(px, py));

                map.PopulationCenters.Add(new Vector2(px, py));
            }
        }
    }
}
