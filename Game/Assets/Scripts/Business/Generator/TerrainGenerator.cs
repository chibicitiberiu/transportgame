﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Business;
using TransportGame.Model;
using TransportGame.Noise;
using TransportGame.Utils;
using TransportGame.Utils.Algorithms;
using UnityEngine;

namespace TransportGame.Generator
{
    public class TerrainGenerator
    {
        /// <summary>
        /// Gets or sets the underlying noise generator
        /// </summary>
        public NoiseGenerator Noise { get; set; }

        private System.Random random = new System.Random();

        public TerrainGenerator()
        {
            Noise = new PerlinNoiseGenerator();

            if (ConfigManager.Tergen == null)
                throw new Exception("Not initialized!");

            Noise.Octaves = ConfigManager.Tergen.NoiseOctaves;
            Noise.NonLinearPower = ConfigManager.Tergen.NoiseNonLinearPower;
            Noise.Scale = ConfigManager.Tergen.ElevationScale;
        }

        public CityMap Generate(int width, int height)
        {
            // Create map
            CityMap map = new CityMap(width, height);

            // Pick a random biome
            map.Biome = PickBiome();
            Logger.Info("Picked biome: {0}", map.Biome.Name);

            // Generate elevation
            GenerateElevation(map);

            // Generate water level
            float waterAmount = random.NextSingle(map.Biome.Moisture.Min, map.Biome.Moisture.Max);
            map.WaterLevel = Mathf.Pow(waterAmount, ConfigManager.Tergen.WaterNonLinearPower) * map.Biome.Height;

            return map;
        }

        private Biome PickBiome()
        {
            int biomeCount = BiomeManager.Biomes.Count();
            int biome = random.Next(biomeCount);

            return BiomeManager.Biomes.ElementAt(biome);
        }

        private void GenerateElevation(CityMap map)
        {
            for (int x = 0; x < map.Width; ++x)
                for (int y = 0; y < map.Height; ++y)
                    map.Heightmap[x, y] = Noise.Generate(x, y, 0, 1);
        }
    }
}
