﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TransportGame.Noise
{
    public class PerlinNoiseGenerator : NoiseGenerator
    {
        protected override float GenerateNoise(float x, float y)
        {
            return Mathf.PerlinNoise(x, y) * 2 - 1.0f;
        }
    }
}
