﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Model.Config;
using TransportGame.Utils;

namespace TransportGame.Business
{
    public static class ConfigManager
    {
        public static readonly string BiomeDirectory = "Assets\\Data\\Biomes";
        public static readonly string ConfigurationDirectory = "Assets\\Data\\Config";

        public static readonly string TerrGenConfigFile = "tergen.xml";
        public static readonly string RoadGenConfigFile = "roadgen.xml";

        public static TerrainGeneratorConfig Tergen { get; private set; }
        public static RoadGeneratorConfig Roadgen { get; private set; }
        public static BuildingGeneratorConfig Buildgen { get; private set; }

        public static void LoadConfiguration()
        {
            // Terrain generator
            string tergenPath = Path.Combine(ConfigurationDirectory, TerrGenConfigFile);
            if (File.Exists(tergenPath))
            {
                Tergen = SerializationHelper.DeserializeXml<TerrainGeneratorConfig>(Path.Combine(ConfigurationDirectory, TerrGenConfigFile));
            }
            else
            {
                Tergen = new TerrainGeneratorConfig();
                Tergen.SerializeXml(tergenPath);
            }

            // Road generator
            string roadgenPath = Path.Combine(ConfigurationDirectory, RoadGenConfigFile);
            if (File.Exists(roadgenPath))
            {
                Roadgen = SerializationHelper.DeserializeXml<RoadGeneratorConfig>(Path.Combine(ConfigurationDirectory, RoadGenConfigFile));
            }
            else
            {
                Roadgen = new RoadGeneratorConfig();
                Roadgen.SerializeXml(roadgenPath);
            }

            // Building generator
            Buildgen = new BuildingGeneratorConfig();
        }
    }
}

