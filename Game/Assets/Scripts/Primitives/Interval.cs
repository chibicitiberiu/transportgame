﻿using System;
using System.Xml.Serialization;

namespace TransportGame.Primitives
{
    /// <summary>
    /// Interval
    /// </summary>
    public class Interval
    {
        #region Properties

        /// <summary>
        /// Gets the minimum value of the interval
        /// </summary>
        [XmlAttribute("min")]
        public float Min { get; set; }

        /// <summary>
        /// Gets the maximum value of the interval
        /// </summary>
        [XmlAttribute("max")]
        public float Max { get; set; }

        #endregion

        #region Other properties

        /// <summary>
        /// Gets or sets the length of the interval
        /// </summary>
        [XmlIgnore]
        public float Length
        {
            get
            {
                return Max - Min;
            }
            set
            {
                Max = Min + value;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes interval
        /// </summary>
        public Interval()
        {
            Min = 0;
            Max = 1;
        }

        /// <summary>
        /// Initializes interval
        /// </summary>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        public Interval(float min, float max)
        {
            Min = min;
            Max = max;
        }

        #endregion

        #region Operations

        /// <summary>
        /// Tests if interval contains value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Contains(float value)
        {
            return (value >= Min && value <= Max);
        }

        #endregion

        #region Object overrides

        /// <summary>
        /// Gets string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("[{0}, {1}]", Min, Max);
        }

        #endregion
    }
}
