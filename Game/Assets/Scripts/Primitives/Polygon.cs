﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TransportGame.Primitives
{
    /// <summary>
    /// Represents a polygon
    /// </summary>
    public class Polygon : IPositionable
    {
        #region Private data types

        private struct Edge
        {
            public int U, V;

            public Edge(int U, int V)
            {
                this.U = U;
                this.V = V;
            }
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the points that define the polygon
        /// </summary>
        public Vector2[] Points { get; set; }

        #endregion

        #region Other properties

        /// <summary>
        /// Gets the gravitational center
        /// </summary>
        public Vector2 Position
        {
            get { return Points.Aggregate((x, y) => x + y) / Points.Length; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes polygon
        /// </summary>
        public Polygon()
        {
            Points = new Vector2[0];
        }

        /// <summary>
        /// Initializes polygon using given points
        /// </summary>
        /// <param name="points">Points</param>
        public Polygon(IEnumerable<Vector2> points)
        {
            Points = points.ToArray();
        }

        /// <summary>
        /// Initializes polygon using given points
        /// </summary>
        /// <param name="points">Points</param>
        public Polygon(params Vector2[] points)
        {
            Points = points;
        }

        #endregion

        #region Operations

        /// <summary>
        /// Returns the union between given polygons
        /// </summary>
        /// <param name="polys">Polygons</param>
        /// <returns>Polygon representing union</returns>
        public static Polygon Union(params Polygon[] polys)
        {
            return Union(polys);
        }

        /// <summary>
        /// Returns the union between given polygons
        /// </summary>
        /// <param name="polys">Polygons</param>
        /// <returns>Polygon representing union</returns>
        public static Polygon Union(IEnumerable<Polygon> polys)
        {
            List<Vector2> vertices = new List<Vector2>();
            List<Edge> edges = new List<Edge>();
            List<Vector2> union = new List<Vector2>();

            foreach (var poly in polys)
            {
                // Add all points
                for (int i = 0; i < poly.Points.Length; i++)
                {
                    int j = (i + 1) % poly.Points.Length;

                    // Get/add first point
                    int indexi = vertices.IndexOf(poly.Points[i]);
                    if (indexi == -1)
                    {
                        vertices.Add(poly.Points[i]);
                        indexi = vertices.Count - 1;
                    }

                    // Get/add second point
                    int indexj = vertices.IndexOf(poly.Points[j]);
                    if (indexj == -1)
                    {
                        vertices.Add(poly.Points[j]);
                        indexj = vertices.Count - 1;
                    }

                    // Add edge
                    edges.Add(new Edge(indexi, indexj));
                }
            }

            // Intersect edges
            for (int i = 0; i < edges.Count; i++)
                for (int j = i + 1; j < edges.Count; j++)
                {
                    LineSegment a = new LineSegment(vertices[edges[i].U], vertices[edges[i].V]);
                    LineSegment b = new LineSegment(vertices[edges[j].U], vertices[edges[j].V]);

                    var inters = LineSegment.Intersect(a, b);
                    if (inters.HasValue && inters.Value != a.P0 && inters.Value != a.P1 && inters.Value != b.P0 && inters.Value != b.P1)
                    {
                        vertices.Add(inters.Value);
                        int index = vertices.Count - 1;

                        edges.Add(new Edge(index, edges[i].V));
                        edges[i] = new Edge(edges[i].U, index);
                        edges.Add(new Edge(index, edges[j].V));
                        edges[j] = new Edge(edges[j].U, index);
                    }
                }

            // Compute union
            int start = 0;

            // Find starting point
            for (int i = 0; i < vertices.Count; i++)
                if (vertices[i].X + vertices[i].Y < vertices[start].X + vertices[start].Y)
                    start = i;

            int v = start, vold = -1;
            Vector2 prev = vertices[v].Normalized; 

            do
            {
                union.Add(vertices[v]);

                int newV = -1;
                Vector2 smallestDir = Vector2.Zero;

                foreach (var edge in edges)
                {
                    if ((edge.U == v || edge.V == v) && edge.V != vold && edge.U != vold)
                    {
                        int otherv = (edge.U == v) ? edge.V : edge.U;
                        Vector2 dir = (vertices[otherv] - vertices[v]).Normalized;

                        // Find smallest angle
                        if (newV == -1 || Vector2.TrigonometricComparer.Compare(dir, smallestDir) < 0)
                        {
                            newV = otherv;
                            smallestDir = dir;
                        }
                    }
                }

                // Advance
                prev = smallestDir;
                vold = v;
                v = newV;

            } while (v != start);

            return new Polygon(union);
        }

        /// <summary>
        /// Tests if two polygons intersect
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Intersect(Polygon a, Polygon b)
        {
            foreach (var poly in new[] { a, b })
            {
                for (int i = 0; i < poly.Points.Length; i++)
                {
                    int j = (i + 1) % poly.Points.Length;

                    var normal = new Vector2(poly.Points[j].Y - poly.Points[i].Y, poly.Points[i].X - poly.Points[j].X);

                    double? minA = null, maxA = null;
                    foreach (var p in a.Points)
                    {
                        var projected = Vector2.Dot(normal, p);
                        if (minA == null || projected < minA)
                            minA = projected;
                        if (maxA == null || projected > maxA)
                            maxA = projected;
                    }

                    double? minB = null, maxB = null;
                    foreach (var p in b.Points)
                    {
                        var projected = Vector2.Dot(normal, p);
                        if (minB == null || projected < minB)
                            minB = projected;
                        if (maxB == null || projected > maxB)
                            maxB = projected;
                    }

                    if (maxA <= minB || maxB <= minA)
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Tests if polygon contains point
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool Contains(Vector2 point)
        {
            // Quick bounding box check
            Vector2 min = Points[0], max = Points[0];

            for (int i = 0; i < Points.Length; i++)
            {
                min.X = Math.Min(min.X, Points[i].X);
                min.Y = Math.Min(min.Y, Points[i].Y);
                max.X = Math.Max(max.X, Points[i].X);
                max.Y = Math.Max(max.Y, Points[i].Y);
            }

            if (point.X < min.X || point.Y < min.Y || point.X > max.X || point.Y > max.Y)
                return false;

            // Ray casting method
            int j = Points.Length - 1;
            bool result = false;
            for (int i = 0; i < Points.Length; i++)
            {
                if ((Points[i].Y > point.Y) != (Points[j].Y > point.Y) &&
                    (point.X < (Points[j].X - Points[i].X) * (point.Y - Points[i].Y) / (Points[j].Y - Points[i].Y) + Points[i].X))
                    result = !result;

                j = i;
            }

            return result;
        }

        #endregion
    }
}
