﻿using System;

namespace TransportGame.Primitives
{
    /// <summary>
    /// Rectangle
    /// </summary>
    public struct Rectangle
    {
        #region Properties

        /// <summary>
        /// Gets or sets the left boundary
        /// </summary>
        public float Left { get; set; }

        /// <summary>
        /// Gets or sets the top boundary
        /// </summary>
        public float Bottom { get; set; }

        /// <summary>
        /// Gets or sets the right boundary
        /// </summary>
        public float Right { get; set; }

        /// <summary>
        /// Gets or sets the bottom boundary
        /// </summary>
        public float Top { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes this rectangle
        /// </summary>
        /// <param name="left">Left boundary</param>
        /// <param name="bottom">Bottom boundary</param>
        /// <param name="right">Right boundary</param>
        /// <param name="top">Top boundary</param>
        public Rectangle(float left, float bottom, float right, float top)
            : this()
        {
            Left = left;
            Bottom = bottom;
            Right = right;
            Top = top;

            if (left > right)
                throw new ArgumentException("Left must be smaller than right.");

            if (bottom > top)
                throw new ArgumentException("Bottom must be smaller than top.");
        }

        #endregion

        #region Other properties

        /// <summary>
        /// Gets or sets the width of the rectangle
        /// </summary>
        public float Width
        {
            get
            {
                return Right - Left;
            }
            set
            {
                Right = Left + value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the rectangle
        /// </summary>
        public float Height
        {
            get
            {
                return Top - Bottom;
            }
            set
            {
                Top = Bottom + value;
            }
        }

        /// <summary>
        /// Gets the area of the rectangle
        /// </summary>
        public float Area
        {
            get
            {
                return Width * Height;
            }
        }

        #endregion

        #region Operations

        /// <summary>
        /// Tests if rectangle contains given point
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <returns>True if point is inside</returns>
        public bool Contains(float x, float y)
        {
            return x >= Left && x <= Right && y >= Bottom && y <= Top;
        }

        /// <summary>
        /// Tests if rectangle contains given point
        /// </summary>
        /// <param name="p">Vector</param>
        /// <returns>True if point is inside</returns>
        public bool Contains(Vector2 p)
        {
            return Contains(p.X, p.Y);
        }

        /// <summary>
        /// Tests if two rectangles intersect
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>True if rectangles intersect</returns>
        public static bool Intersect (Rectangle a, Rectangle b)
        {
            return !(b.Left > a.Right ||
                     b.Right < a.Left ||
                     b.Bottom > a.Top ||
                     b.Top < a.Bottom);
        }
        #endregion

        #region Object overrides

        /// <summary>
        /// Gets string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("({0}, {1}, {2}, {3})", Left, Bottom, Right, Top);
        }

        #endregion
    }
}
