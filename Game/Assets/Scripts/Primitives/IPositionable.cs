﻿
namespace TransportGame.Primitives
{
    /// <summary>
    /// Object can be positioned using a point
    /// </summary>
    public interface IPositionable
    {
        /// <summary>
        /// Gets the position of the object
        /// </summary>
        Vector2 Position { get; }
    }
}
