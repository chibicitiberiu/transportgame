﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportGame.Utils
{
    public static class RandomExtensions
    {
        public static float NextSingle(this Random @this)
        {
            return Convert.ToSingle(@this.NextDouble());
        }

        public static float NextSingle(this Random @this, float minValue, float maxValue)
        {
            return @this.NextSingle() * (maxValue - minValue) + minValue;
        }
    }
}
