﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportGame.Utils
{
    public static class MathHelper
    {
        /// <summary>
        /// Clamps given value in the [0,1] interval
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>Clamped value</returns>
        public static float Clamp01(float value)
        {
            return Clamp(value, 0, 1);
        }

        /// <summary>
        /// Clamps given value in the [min,max] interval
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns>Clamped value</returns>
        public static float Clamp(float value, float min, float max)
        {
            if (value < min)
                return min;

            if (value > max)
                return max;

            return value;
        }
    }
}
