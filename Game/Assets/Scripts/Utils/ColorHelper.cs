﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TransportGame.Utils
{
    public static class ColorHelper
    {
        public static Color FromArgb(int argb)
        {
            float a = ((argb >> 24) & 0xff) / 255f;
            float r = ((argb >> 16) & 0xff) / 255f;
            float g = ((argb >> 8) & 0xff) / 255f;
            float b = ((argb) & 0xff) / 255f;

            return new Color(r, g, b, a);
        }
    }
}
