﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Primitives;

namespace TransportGame.Model.Road
{
    /// <summary>
    /// Road node
    /// </summary>
    [XmlRoot("node")]
    public class RoadNode : IPositionable
    {
        /// <summary>
        /// Gets or sets a unique identifier for this node
        /// </summary>
        [XmlAttribute("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the X coordinate of the node
        /// </summary>
        [XmlAttribute("x")]
        public float X { get; set; }

        /// <summary>
        /// Gets or sets the Y coordinate of the node
        /// </summary>
        [XmlAttribute("y")]
        public float Y { get; set; }

        /// <summary>
        /// Gets or sets the adjacent articulation segment IDs
        /// </summary>
        [XmlArray("articulationSegments")]
        [XmlArrayItem("id")]
        public List<int> ArticulationSegmentIds { get; set; }

        /// <summary>
        /// Gets or sets the adjacent articulation segment IDs
        /// </summary>
        [XmlArray("intersectionSegments")]
        [XmlArrayItem("id")]
        public List<int> IntersectionSegmentIds { get; set; }

        /// <summary>
        /// Gets or sets the parent network
        /// </summary>
        [XmlIgnore]
        public RoadNetwork ParentNetwork { get; set; }

        /// <summary>
        /// Gets the adjacent articulation segments
        /// </summary>
        [XmlIgnore]
        public IEnumerable<RoadSegment> ArticulationSegments
        {
            get
            {
                return ArticulationSegmentIds.Select(id => ParentNetwork.ArticulationSegments[id]);
            }
        }

        /// <summary>
        /// Gets the adjacent segments
        /// </summary>
        [XmlIgnore]
        public IEnumerable<RoadSegment> Segments
        {
            get
            {
                return IntersectionSegmentIds.Select(id => ParentNetwork.ArticulationSegments[id]);
            }
        }

        /// <summary>
        /// Initializes the node
        /// </summary>
        public RoadNode()
        {
            ArticulationSegmentIds = new List<int>();
            IntersectionSegmentIds = new List<int>();
        }

        /// <summary>
        /// Gets or sets the position
        /// </summary>
        [XmlIgnore]
        public Vector2 Position
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public override string ToString()
        {
            return string.Format("(node id={0}, {1})", Id, Position);
        }
    }
}
