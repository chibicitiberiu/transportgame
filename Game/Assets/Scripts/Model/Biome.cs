﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Primitives;
using TransportGame.Utils;

namespace TransportGame.Model
{
    [XmlRoot("biome")]
    public class Biome
    {
        /// <summary>
        /// Gets or sets the name of the biome
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the maximum height of the biome.
        /// </summary>
        /// <remarks>
        /// 1 unit = 100 meters.
        /// </remarks>
        [XmlElement("height")]
        public float Height { get; set; }
        
        /// <summary>
        /// Gets or sets the moisture range.
        /// </summary>
        /// <remarks>
        /// Moisture is the amount of water on a map.
        /// Value is a probability, should be between 0 and 1.
        /// </remarks>
        [XmlElement("moisture")]
        public Interval Moisture { get; set; }

        /// <summary>
        /// Gets or sets the vegetation density of the biome
        /// </summary>
        [XmlElement("vegetationDensity")]
        public Interval VegetationDensity { get; set; }

        /// <summary>
        /// Gets or sets an array of textures to use
        /// </summary>
        [XmlArray("textures")]
        [XmlArrayItem("texture")]
        public Texture[] Textures { get; set; }

        /// <summary>
        /// Gets or sets the biome file name
        /// </summary>
        [XmlIgnore]
        public string FileName { get; set; }
    }
}
