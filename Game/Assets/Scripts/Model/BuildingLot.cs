﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportGame.Business;
using TransportGame.Primitives;
using TransportGame.Utils;

namespace TransportGame.Model
{
    /// <summary>
    /// Building lot
    /// </summary>
    public class BuildingLot : Polygon
    {
        /// <summary>
        /// Gets or sets the size of the lot
        /// </summary>
        public float Size { get; set; }

        /// <summary>
        /// Initializes the building lot
        /// </summary>
        public BuildingLot()
            : base(new Vector2[4])
        {
        }

        /// <summary>
        /// Initializes lot with given size
        /// </summary>
        /// <param name="size">size</param>
        public BuildingLot(float size)
            : base(new Vector2[4])
        {
            Size = size;
        }

        /// <summary>
        /// Initializes lot with given points and size
        /// </summary>
        /// <param name="size">Size</param>
        /// <param name="points">Points</param>
        public BuildingLot(float size, params Vector2[] points)
            : base(points)
        {
            Size = size;
        }

        /// <summary>
        /// Initializes lot with given points and size
        /// </summary>
        /// <param name="size">Size</param>
        /// <param name="points">Points</param>
        public BuildingLot(float size, IEnumerable<Vector2> points)
            : base(points)
        {
            Size = size;
        }

        /// <summary>
        /// Tests if a building lot intersects a line segment (such as a road segment)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool DoesIntersect(BuildingLot a, LineSegment b)
        {
            for (int i = 0; i < a.Points.Length; i++)
            {
                int j = (i + 1 >= a.Points.Length) ? 0 : i + 1;
                
                LineSegment s = new LineSegment(a.Points[i], a.Points[j]);
                if (LineSegment.Intersect(s, b).HasValue)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Tests if a point is inside the building lot
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsInside(Vector2 point)
        {
            return IsInsideTriangle(point, Points[0], Points[1], Points[2]) || IsInsideTriangle(point, Points[0], Points[2], Points[3]);
        }

        private static float Sign(Vector2 a, Vector2 b, Vector2 c)
        {
            return (a.X - c.X) * (b.Y - c.Y) - (b.X - c.X) * (a.Y - c.Y);
        }

        private static bool IsInsideTriangle(Vector2 pt, Vector2 a, Vector2 b, Vector2 c)
        {
            bool b1 = Sign(pt, a, b) < 0;
            bool b2 = Sign(pt, b, c) < 0;
            bool b3 = Sign(pt, c, a) < 0;

            return (b1 == b2) && (b2 == b3);
        }
    }
}
