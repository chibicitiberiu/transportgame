﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TransportGame.Model
{
    [XmlRoot("texture")]
    public class Texture
    {
        [XmlAttribute("src")]
        public string Source { get; set; }
        
        [XmlAttribute("expr")]
        public string Expression { get; set; }
    }
}
