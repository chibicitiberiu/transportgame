﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportGame.Model.Config
{
    public class BuildingGeneratorConfig
    {
        /// <summary>
        /// Minimum lot size
        /// </summary>
        public float LotSquareMinSize { get; set; }

        /// <summary>
        /// Maximum lot size
        /// </summary>
        public float LotSquareMaxSize { get; set; }

        /// <summary>
        /// Space between lots
        /// </summary>
        public float LotSpacing { get; set; }

        /// <summary>
        /// Maximum number of attempts to generate a lot in a specific area
        /// </summary>
        public int MaxLotAttempts { get; set; }

        /// <summary>
        /// Maximum number of levels on a building
        /// </summary>
        public int MaxBuildingLevels { get; set; }

        /// <summary>
        /// Maximum height for a building
        /// </summary>
        public float MaxLevelHeight { get; set; }
        public float MinLevelHeight { get; set; }
        
        /// <summary>
        /// Maximum number of primitive polygons to be generated per level
        /// </summary>
        public int MaxPolygonsPerLevel { get; set; }

        public BuildingGeneratorConfig()
        {
            LotSquareMinSize = 5f;
            LotSquareMaxSize = 20f;
            LotSpacing = 0.1f;
            MaxLotAttempts = 3;
            MaxLevelHeight = 20f;
            MinLevelHeight = 5f;
            MaxBuildingLevels = 7;
            MaxPolygonsPerLevel = 2;
        }
    }
}
