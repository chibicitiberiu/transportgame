﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportGame.Model.Config
{
    public class RoadGeneratorConfig
    {
        /// <summary>
        /// Gets or sets the default segment length
        /// </summary>
        public float DefaultSegmentLength { get; set; }

        /// <summary>
        /// Gets or sets the highway segment length
        /// </summary>
        public float HighwaySegmentLength { get; set; }

        public float DefaultBranchProbability { get; set; }
        public float DefaultBranchPopulationTreshold { get; set; }
        public float HighwayBranchProbability { get; set; }
        public float HighwayBranchPopulationTreshold { get; set; }
        
        public int HighwayBranchDelay { get; set; }

        public float SteepnessLimit { get; set; }
        public float SlopeLimit { get; set; }
        public float RoadSegmentAngleLimit { get; set; }
        public float MinNodeDistance { get; set; }
        
        public float RoadSnapDistance { get; set; }
        public int MaximumRandomStraightAngle { get; set; }
        public int MaximumBranchAngleVariation { get; set; }
        public int MaximumIntersectingRoads { get; set; }

        public float SidewalkWidth { get; set; }
        public float LaneWidth { get; set; }

        /// <summary>
        /// Raises road above actual height of terrain
        /// </summary>
        public float RaiseOffset { get; set; }

        /// <summary>
        /// Height of sidewalk
        /// </summary>
        public float SidewalkHeight { get; set; }

        /// <summary>
        /// On the sides of the roads, so that we can't see under the road
        /// </summary>
        public float SideCoverHeight { get; set; }

        /// <summary>
        /// Initializes configuration with default values
        /// </summary>
        public RoadGeneratorConfig()
        {
            HighwaySegmentLength = 60;
            DefaultBranchPopulationTreshold = 0.12f;
            DefaultBranchProbability = 0.2f;
            DefaultSegmentLength = 24;
            SteepnessLimit = 10;
            SlopeLimit = (float)Math.PI / 7;
            RoadSegmentAngleLimit = (float)Math.PI / 4;
            RoadSnapDistance = 19;
            MinNodeDistance = 12;
            MaximumRandomStraightAngle = 45; // in degrees
            MaximumBranchAngleVariation = 12; // in degrees
            HighwayBranchPopulationTreshold = .4f; // 0..1
            HighwayBranchProbability = .01f;
            HighwayBranchDelay = 3;
            MaximumIntersectingRoads = 5;

            SidewalkWidth = 1f;
            LaneWidth = 1.3f;
            RaiseOffset = 0.8f;
            SidewalkHeight = 0.1f;
            SideCoverHeight = 0.4f;
        }
    }
}
