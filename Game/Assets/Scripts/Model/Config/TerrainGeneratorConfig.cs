﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TransportGame.Model.Config
{
    [XmlRoot("terrgenConfig")]
    public class TerrainGeneratorConfig
    {
        [XmlElement("noiseOctaves")]
        public int NoiseOctaves { get; set; }

        [XmlElement("noiseNonLinearPower")]
        public float NoiseNonLinearPower { get; set; }

        [XmlElement("elevationScale")]
        public float ElevationScale { get; set; }

        [XmlElement("waterNonLinearPower")]
        public float WaterNonLinearPower { get; set; }

        [XmlElement("erodePoints")]
        public int ErodePoints { get; set; }

        [XmlElement("erodeIterations")]
        public int ErodeIterations { get; set; }

        [XmlElement("erodeAmountPercent")]
        public float ErodeAmountPercent { get; set; }

        /// <summary>
        /// Initializes with default values
        /// </summary>
        public TerrainGeneratorConfig()
        {
            NoiseOctaves = 9;
	        NoiseNonLinearPower = 2.4f;
	        WaterNonLinearPower = 1.9f;
	        ElevationScale = 0.001f;
	        ErodePoints = 100;
	        ErodeIterations = 60;
	        ErodeAmountPercent = 0.1f;
        }
    }
}
