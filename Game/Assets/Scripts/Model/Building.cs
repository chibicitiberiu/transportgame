﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportGame.Primitives;

namespace TransportGame.Model
{
    public class Building
    {
        /// <summary>
        /// From lowest to highest level
        /// </summary>
        public Polygon[][] Polygons { get; set; }

        /// <summary>
        /// From lowest to highest level
        /// </summary>
        public float[] LevelHeights { get; set; }
    }
}
