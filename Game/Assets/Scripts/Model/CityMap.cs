﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TransportGame.Model.Road;
using TransportGame.Primitives;
using TransportGame.Utils;

namespace TransportGame.Model
{
    [XmlRoot("map")]
    public class CityMap
    {
        private float[,] heightmap;

        #region Properties
        
        /// <summary>
        /// Gets or sets the biome
        /// </summary>
        [XmlElement("biome")]
        public Biome Biome { get; set; }

        /// <summary>
        /// Gets or sets the water level
        /// </summary>
        [XmlElement("waterLevel")]
        public float WaterLevel { get; set; }

        /// <summary>
        /// Gets the heights array in range [0,1]
        /// </summary>
        [XmlIgnore()]
        public float[,] Heightmap
        {
            get
            {
                return heightmap;
            }
        }

        /// <summary>
        /// Gets or sets the heights as raw bytes
        /// </summary>
        [XmlElement("heightmap")]
        public byte[] HeightmapRaw
        {
            get
            {
                return heightmap.ToByteArray();
            }
            set
            {
                heightmap = value.GetFloatMatrix();
            }
        }

        /// <summary>
        /// Gets width of heightmap
        /// </summary>
        [XmlIgnore]
        public int Width { get { return (heightmap == null) ? 0 : heightmap.GetLength(0); } }

        /// <summary>
        /// Gets height of heightmap
        /// </summary>
        [XmlIgnore]
        public int Height { get { return (heightmap == null) ? 0 : heightmap.GetLength(1); } }

        /// <summary>
        /// Gets or sets the population map
        /// </summary>
        [XmlArray("populationCenters")]
        [XmlArrayItem("center")]
        public List<Vector2> PopulationCenters { get; set; }

        /// <summary>
        /// Gets or sets the range of one population center (distance how far it influences)
        /// </summary>
        public float PopulationCenterRange { get; set; }

        /// <summary>
        /// Gets or sets the articulation road network
        /// </summary>
        [XmlElement("roadNetwork")]
        public RoadNetwork RoadNetwork { get; set; }

        /// <summary>
        /// Gets or sets the building lots
        /// </summary>
        [XmlElement("lots")]
        public List<BuildingLot> BuildingLots { get; set; }

        /// <summary>
        /// Gets or sets the buildings
        /// </summary>
        [XmlElement("buildings")]
        public List<Building> Buildings { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes the map
        /// </summary>
        /// <remarks>
        /// Warning: heights array will be null.
        /// </remarks>
        public CityMap()
        {
            PopulationCenters = new List<Vector2>();
        }

        /// <summary>
        /// Initializes the map
        /// </summary>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        public CityMap(int width, int height)
        {
            heightmap = new float[width, height];
            PopulationCenters = new List<Vector2>();
        }

        #endregion

        /// <summary>
        /// Gets the cell at specified position in range [0, Biome.Height]
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>Value</returns>
        public float GetHeight(int x, int y)
        {
            return heightmap[x, y] * Biome.Height;
        }

        /// <summary>
        /// Sets the height at specified position in range [0, Biome.Height]
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <param name="value">Value</param>
        public void SetHeight(int x, int y, float value)
        {
            heightmap[x, y] = value / Biome.Height;
        }

        /// <summary>
        /// Returns true if specified cell is a water cell
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns></returns>
        public bool IsWater(float x, float y)
        {
            return GetHeight((int)x, (int)y) <= WaterLevel;
        }

        /// <summary>
        /// Returns true if specified cell is a water cell
        /// </summary>
        /// <param name="p">Position vector</param>
        /// <returns></returns>
        public bool IsWater(Vector2 p)
        {
            return IsWater(p.X, p.Y);
        }

        /// <summary>
        /// Returns true if given coordinates is inside the map
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>True if coordinates are inside the map</returns>
        public bool IsInside(float x, float y)
        {
            return x >= 0 && y >= 0 && x < Width && y < Height;
        }

        /// <summary>
        /// Returns true if given coordinates is inside the map
        /// </summary>
        /// <param name="p">Position vector</param>
        /// <returns>True if coordinates are inside the map</returns>
        public bool IsInside(Vector2 p)
        {
            return IsInside(p.X, p.Y);
        }

        /// <summary>
        /// Gets steepness in specified point
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>Steepness</returns>
        public float GetSteepness(int x, int y)
        {
            if (x == 0) x++;
            if (y == 0) y++;

            float dx = GetHeight(x - 1, y) - GetHeight(x, y);
            float dy = GetHeight(x, y - 1) - GetHeight(x, y);
            
            return dx * dx + dy * dy;
        }

        /// <summary>
        /// Gets steepness in specified point
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>Steepness</returns>
        public float GetSteepness(float x, float y)
        {
            return GetSteepness((int)x, (int)y);
        }

        /// <summary>
        /// Gets steepness in specified point
        /// </summary>
        /// <param name="pos">Position</param>
        /// <returns>Steepness</returns>
        public float GetSteepness(Vector2 pos)
        {
            return GetSteepness((int)pos.X, (int)pos.Y);
        }

        /// <summary>
        /// Gets population using terrain coordinates
        /// </summary>
        /// <param name="x">X</param>
        /// <param name="y">Y</param>
        /// <returns>Population</returns>
        public float GetPopulation(float x, float y)
        {
            return GetPopulation(new Vector2(x, y));
        }

        /// <summary>
        /// Gets population using terrain coordinates
        /// </summary>
        /// <param name="p">Position</param>
        /// <returns>Population</returns>
        public float GetPopulation(Vector2 p)
        {
            float value = 0.05f;

            foreach (var point in PopulationCenters)
            {
                Vector2 diff = p - point;
                float dist = diff.LengthSq;

                if (dist < PopulationCenterRange * PopulationCenterRange)
                {
                    float influence = 1 - (float)dist / (float)(PopulationCenterRange * PopulationCenterRange);
                    influence = (float)Math.Pow(influence, 6) * 0.7f; // Ease
                    value = MathHelper.Clamp01(value + influence);
                }
            }

            return value;
        }
    }
}
