﻿using System.Collections;
using System.Threading;
using TransportGame.Business;
using TransportGame.Model;
using TransportGame.Utils;
using UnityEngine;

public class InitializeScript : MonoBehaviour
{
    // Use this for initialization
    public void Start()
    {        
        // Load configuration
        Logger.Info("Loading configuration...");
        ConfigManager.LoadConfiguration();

        // Load biomes
        Logger.Info("Loading biomes...");
        BiomeManager.LoadBiomes();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
